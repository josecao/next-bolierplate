import React, {useContext} from 'react'
import PropTypes from 'prop-types'

import UserContext from '../context/user';


const test = props => {
    const user = useContext(UserContext);
    console.log(user);
    return (
        <div>
            <button onClick={()=> user.inCount()}>test</button>
        </div>
    )
}

test.propTypes = {

}

export default test
